#!/usr/bin/env node
/*
 * build script
 */

const fs = require("fs");
const path = require("path");
const { exec } = require('child_process');

const PUBLIC_URL = "https://1guter.de/";

const INPUT_DIR = "./pages/";
const OUTPUT_DIR = "./public/";

const TEMPLATE_FILE = "./template.html";

const SITEMAP_FILE = path.join(OUTPUT_DIR, "sitemap.xml");
// 0.5 is default
const SITEMAP_PRIOS = {
  "index.html": "0.75",
  "impressum.html": "0.25",
};

const ROBOTS_TXT_FILE = path.join(OUTPUT_DIR, "robots.txt");

async function readTemplate() {
  return new Promise((resolve, reject) => {
    fs.readFile(TEMPLATE_FILE, "utf8", (err, data) => {
      if (err) {
        reject(err);
      }

      resolve(data);
    });
  });
}

async function build() {
  const template = await readTemplate();

  let sitemap = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 https://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">\n`;
  const now = new Date().toISOString();

  fs.readdir(INPUT_DIR, (err, files) => {
    if (err) {
      throw err;
    }

    files.forEach((file) => {
      const filePath = path.join(INPUT_DIR, file);
      fs.readFile(filePath, "utf8", (err, data) => {
        if (err) {
          throw err;
        }

        const output = template.replace("{{ content }}", data);
        const outputFilePath = path.join(OUTPUT_DIR, file);

        fs.writeFile(outputFilePath, output, (err) => {
          if (err) {
            throw err;
          }
          console.log(`${filePath} \t-> ${outputFilePath}`);
        });
      });

      // TODO
      sitemap += `  <url>
    <loc>${PUBLIC_URL}${file}</loc>
    <lastmod>${now}</lastmod>
    <priority>${SITEMAP_PRIOS[file] || "0.5"}</priority>
  </url>
`;
    });

    sitemap += "</urlset>\n";
    fs.writeFile(SITEMAP_FILE, sitemap, (err) => {
      if (err) {
        throw err;
      }
      console.log(`Sitemap written to: ${SITEMAP_FILE}`);
    });

    fs.writeFile(
      ROBOTS_TXT_FILE,
      `User-agent: *
Disallow:
Sitemap: ${PUBLIC_URL}sitemap.xml
`,
      (err) => {
        if (err) {
          throw err;
        }
        console.log(`robots.txt written to: ${ROBOTS_TXT_FILE}`);
      }
    );

    exec("curl --silent -L -o ./public/profile-picture-generator.js https://unpkg.com/profile-picture-generator@1.5.0/dist/profile-picture-generator.js", (err, stdout, stderr) => {
      if (err) {
        console.error(`Could not download pbg: ${err}`);
        process.exit(1);
      }

      // the *entire* stdout and stderr (buffered)
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      exec("sha512sum ./public/profile-picture-generator.js | grep b3e639c58a505ac43d08e867885f747a593bb50a2b3b6d9e39e8d1bda5a3a579d1906924955950bca6289c63dbb6c56c607de3c1e633e5d770772018d047fe19", (err, stdout, stderr) => {
        if (err) {
          console.error(`Could not verify pbg: ${err}`);
          process.exit(1);
        }

        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
      });
    });
  });
}

build();
